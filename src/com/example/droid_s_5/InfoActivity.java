package com.example.droid_s_5;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class InfoActivity extends Activity {


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_info);
		
		
		TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);
		tabHost.setup();

		tabHost.addTab(createTabSpec(tabHost, "Abc", R.id.tab1, null));
		tabHost.addTab(createTabSpec(tabHost, "Def", R.id.tab2, null));
	}
	
	private TabSpec createTabSpec(TabHost host, String title, int layout, Drawable ico) {
		TabSpec tabSpec = host.newTabSpec(title);
		tabSpec.setContent(layout);
		tabSpec.setIndicator(title, null);
		return tabSpec;
	}

}